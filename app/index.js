const express = require("express");
const passportJWT = require("app/profile/middleware/passportJWT");

const router = express.Router();

router.use("/marketplace", require("./marketplace/index"));
router.use("/user", require("./profile/routes/user"));
router.use("/friends", passportJWT, require("./profile/routes/friends"));
router.use("/account", passportJWT, require("./stellar/routes/account"));
router.use("/anchor/", require("./stellar/routes/anchor"));
router.use("/oauth", require("./oauth/index"));
router.use("/payment-service", require("./payment_service/index"));


router.use("/uploads", express.static('uploads'));
router.use("/common", require("../routes/common"));

/****/

/* Запуск в крон режиме*/
//router.use("/cron", require("../routes/cron"));
//

/* тествые методы */
router.use("/test", require("../routes/test"));
router.use("/bridge", require("../routes/server/bridge"));
router.use("/compliance", require("../routes/server/compliance"));
/****/

module.exports = router;