const appSuccessMessage = require("lib/AppSuccessMessage");

const WebSocketNotice = (socket, wss ,req) => {

    socket.on('message', (message) => {
        console.log({message})
    });

    socket.on('notice', (data) => {
        const msg = appSuccessMessage(data);
        msg.event = 'NOTICE';
        socket.send(JSON.stringify(msg));
    });

    wss.sendNotice = sendNotice;
    const msg = appSuccessMessage({message: 'connection succesfull'});
    socket.send(JSON.stringify(msg));
};

module.exports = WebSocketNotice;


function sendNotice(user, data){
    this.clients.forEach((socket)=>{
        if(socket.user.id == user.id){
            socket.emit('notice',data);
        }
    })
}