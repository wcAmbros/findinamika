const WebSocket = require('ws');
const Url = require('url');
const jwt = require('jsonwebtoken');
const config = require('config');
const jwtBlacklist = require("lib/jwtBlackList");
const appErrorMessage = require("lib/AppErrorMessage");
const socket = {
    server: null,

    init: function (path = null, callback) {
        const server = this.server;

        const wss = new WebSocket.Server({server, path});

        wss.on('connection', function (ws, req) {
            const {query:{access_token}} = Url.parse(req.url, true);
            try {
                jwtBlacklist.verify(access_token);
                const user = jwt.verify(access_token, config.token.secret);
                ws.user = user;
            }catch (e) {
                const msg = appErrorMessage({name:"Error", message:"Error authorized"});
                ws.send(JSON.stringify(msg));
                ws.terminate();
            }

            ws.isAlive = true;
            ws.on('pong', heartbeat);

            callback(ws, wss, req);
        });

        const interval = setInterval(ping, 30000, wss);


        return wss;
    }
}

function noop() {
}

function heartbeat() {
    this.isAlive = true;
}

function ping(wss) {
    wss.clients.forEach(function each(ws) {
        if (ws.isAlive === false) return ws.terminate();
        ws.isAlive = false;
        ws.ping(noop);
    });
}



module.exports = socket;