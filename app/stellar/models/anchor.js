const mongoose = require("mongoose");
const {stellar_toml} = require("config");
const {manageOffer, findOffer, Server, initAccount} = require("../helpers/helper_stellar");
const Issuer = require('./issuer')
const BridgeService = require("../lib/BridgeService")

const Schema = mongoose.Schema;

const AnchorSchema = new Schema(
    {
        publicKey: String,
        secretKey: String,
        stellar_name: String,
        organization_info: Object
    }
)

AnchorSchema.virtual('stellar_address').get(function () {
    return `${this.stellar_name}*${stellar_toml}`;
});

AnchorSchema.methods.info = async function () {
    return Server.loadAccount(this.publicKey);
}

AnchorSchema.methods.burn = async function (currency, amount) {
    const issuer =  await Issuer.findOne({currency});
    const bridgeService = new BridgeService();

    return bridgeService.payment(
        {
            sender:this,
            destination: issuer,
            amount: amount,
            asset: issuer.getAsset(),
            memo: "burn asset"
        })
}

AnchorSchema.methods.createOffer = async function ({buying, selling, amount, price}) {
    const buyIssuer = await Issuer.findOne({currency:buying})
    const sellIssuer = await Issuer.findOne({currency:selling})

    return anchorManageOffer({
        buying: buyIssuer.getAsset(),
        selling:sellIssuer.getAsset(),
        amount,
        price,
        source:this
    });
}

AnchorSchema.methods.updateOffer = async function ({buying, selling, amount, price}) {
    const buyIssuer = await Issuer.findOne({currency:buying})
    const sellIssuer = await Issuer.findOne({currency:selling})

    const offer = await findOffer({
        buying: buyIssuer.getAsset(),
        selling: sellIssuer.getAsset()
    }, this.publicKey)

    return anchorManageOffer({
        buying: buyIssuer.getAsset(),
        selling: sellIssuer.getAsset(),
        amount,
        price,
        offerId: offer.id,
        source:this
    });
}

AnchorSchema.methods.findOffer = async function ({buying, selling}){
    const buyIssuer = await Issuer.findOne({currency:buying})
    const sellIssuer = await Issuer.findOne({currency:selling})

    return findOffer({
        buying: buyIssuer.getAsset(),
        selling:sellIssuer.getAsset()
    }, this.publicKey)
}

AnchorSchema.methods.createStellarAccount = async function (){
    return initAccount(this.secretKey)
}

module.exports = mongoose.model("Anchor", AnchorSchema);

const anchorManageOffer = async ({buying, selling, amount, price, offerId=0, source})=>{
    return manageOffer({
        selling,
        buying,
        amount: amount,
        price: price,
        offerId
    }, source.secretKey, false);
}