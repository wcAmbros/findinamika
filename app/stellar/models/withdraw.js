const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const withdrawSchema = new Schema(
    {
        status: {
            type: String,
            enum : ['unknown','approved','not_approved','pending','failed','refunded','claimable','delivered'],
            default: 'unknown'
        },
    }
);

module.exports = mongoose.model("Withdraw", withdrawSchema);