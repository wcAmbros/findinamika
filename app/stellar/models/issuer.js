const mongoose = require("mongoose");
const StellarSdk = require("stellar-sdk");
const {stellar_toml} = require("config");
const {Server} = require("../helpers/helper_stellar");
const BridgeService = require("../lib/BridgeService")
const Schema = mongoose.Schema;

const IssuerSchema = new Schema(
    {
        publicKey: String,
        secretKey: String,
        currency: {type: String, required: true, unique: true},
        description: String,
        organization_info: Object
    }
)
IssuerSchema.virtual('stellar_address').get(function () {
    return `${this.stellar_name}*${stellar_toml}`;
});

IssuerSchema.virtual('stellar_name').get(function () {
    return this.currency;
});

IssuerSchema.methods.info = async function () {
    return Server.loadAccount(this.publicKey);
}

IssuerSchema.methods.mint = async function ({amount, destination, memo = "mint asset"}) {
    const bridgeService = new BridgeService();
    return bridgeService.payment(
        {
            sender:this,
            destination,
            amount,
            asset: this.getAsset(),
            memo
        })
}

IssuerSchema.methods.getAsset = function (){
    return new StellarSdk.Asset(this.currency, this.publicKey);
}

module.exports = mongoose.model("Issuer", IssuerSchema);