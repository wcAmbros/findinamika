const mongoose = require("mongoose");
const Issuer = require("./issuer");
const {Server, changeTrust, isPublicKey} = require("app/stellar/helpers/helper_stellar");
const history = require("app/stellar/lib/history");
const {stellar_toml} = require("config");

const ComplianceService = require("../lib/ComplianceService")
const BridgeService = require("../lib/BridgeService")
const Schema = mongoose.Schema;

const AccountSchema = new Schema(
    {
        user_id: {
            type: Schema.Types.ObjectId, ref: 'User',
            required: true
        },
        secretKey: {
            type: String,
            required: true
        },
        publicKey: {
            type: String,
            required: true
        },
        destinations: {
            type: Map,
            of: mongoose.Schema.Types.Mixed
        }
    },
    {virtuals: true},
);

AccountSchema.virtual('stellar_address').get(function () {
    return `${this.stellar_name}*${stellar_toml}`;
});

AccountSchema.virtual('stellar_name').get(function () {
    return this._id.toString();
});

AccountSchema.methods.info = async function () {
    return await Server.loadAccount(this.publicKey);
}

AccountSchema.methods.balance = async function () {
    const {balances} = await this.info();
    const asset_alphanum4 = [];
    const asset_alphanum12 = [];
    let asset_native = {};
    balances.forEach((item) => {
        const {asset_type, balance, asset_code, asset_issuer} = item;
        const row = {asset_type, balance, asset_code, asset_issuer};
        if(asset_type == "credit_alphanum4"){
            asset_alphanum4.push(row)
        }
        if(asset_type == "credit_alphanum12"){
            asset_alphanum12.push(row)
        }
        if(item.asset_type == "native"){
            row.asset_code = "XLM";
            row.asset_issuer = "";
            asset_native = row
        }

    });

    return {asset_alphanum4, asset_alphanum12, asset_native}
}

AccountSchema.methods.getDestination = async function (to) {

    if (to.split("*").length == 2) {
        return to;
    }
    if (!isPublicKey(to)) {
        return `${to}*${stellar_toml}`;
    }
    return to;
}

AccountSchema.methods.trustline = async function (currency, limit = "") {
    const issuer = await Issuer.findOne({currency})
    return await changeTrust({asset:issuer.getAsset(), limit}, this.secretKey);
}

AccountSchema.methods.addDestinationInfo = async function (to, transaction) {

    if (!isPublicKey(to)) {
        if (!this.destinations) {
            this.destinations = new Map();
        }
        const {records} = await Server.operations().forTransaction(transaction.hash).call();
        const record = records[0];

        this.destinations.set(record.to, {
            name: to,
            publicKey: record.to,
            last_operation: {
                hash: record.transaction_hash,
                created_at: record.created_at,
                type: record.type,
            }
        });
        await this.save();
    }
}

AccountSchema.methods.payment = async function ({to, currency, amount = 0, memo = "payment"}) {
    const bridgeService = new BridgeService();
    const destination = await getAccountByUser(to);
    const issuer = await Issuer.findOne({currency})

    const transaction = await bridgeService.payment({
        sender: this,
        destination: destination,
        amount: amount,
        asset: issuer.getAsset(currency),
        memo: memo
    });
    this.addDestinationInfo(to, transaction).then();

    return transaction;
}

AccountSchema.methods.paymentPath = async function ({source_currency, to, currency, amount = 0, memo = ""}) {

    const {asset_alphanum4} = await this.balance();
    const row = asset_alphanum4.find((item) => (item.asset_code == source_currency));

    if (!row.balance) {
        throw new Error(`can't find source currency ${source_currency} in account`);
    }

    const bridgeService = new BridgeService();
    const destination = await getAccountByUser(to);
    const issuer = await Issuer.findOne({currency})
    const sourceIssuer = await Issuer.findOne({currency})

    const transaction = await bridgeService.paymentPath({
        sender:this,
        destination: destination,
        amount: amount,
        asset: issuer.getAsset(),
        send_asset: sourceIssuer.getAsset(),
        send_max: row.balance,
        memo: memo
    });

    this.addDestinationInfo(to, transaction).then();
    return transaction;
}

AccountSchema.methods.history = async function ({order = "asc", limit = 50, last_id}) {

    const payments = await Server.payments().order(order).limit(limit).forAccount(this.publicKey);

    if (last_id) {
        payments.cursor(last_id);
    }

    let {records} = await payments.call();

    records = records.map(async (item) => {

        const data = history(item);

        if (item.type == "payment" || item.type == "path_payment") {
            const acc = await Account.findOne({publicKey: item.to}).populate('user_id');
            if (!item.asset_code) {
                item.asset_code = "XLM";
                item.asset_issuer = "";
            }

            const {memo_type, memo} = await item.transaction();

            if (memo_type == 'hash') {
                const complianceService = new ComplianceService()
                const thx = await complianceService.getTransaction(memo);
                item.extra = {
                    sender: thx.data.sender,
                    transaction: thx.data.attachment.transaction,
                };
            }

            if (acc) {
                item.receive_info = {
                    name: acc.user_id.login
                }
            }

            data.extra = item.extra;
            data.receive_info = item.receive_info;
            data.asset_code = item.asset_code;
            data.asset_issuer = item.asset_issuer;
        }

        return data;
    });

    records = await Promise.all(records);

    return {payments: records};
}

const Account = mongoose.model("Account", AccountSchema);


module.exports = Account;

const getAccountByUser = async (login)=>{
    const User = require('app/profile/models/user');
    const user = await User.findOne({login});
    if(user){
        return Account.findOne({user_id: user._id});
    }else{
        throw new Error(`Can't found user by login: ${login}`);
    }
}