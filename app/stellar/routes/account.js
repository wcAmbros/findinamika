const express = require("express");
const Account = require("../models/account");
const appErrorMessage = require("lib/AppErrorMessage");
const appSuccessMessage = require("lib/AppSuccessMessage");
const {withdraw} = require("../helpers/helper_anchor");
const YandexMoney = require("app/payment_service/models/yandexMoney");
const YandexMoneyJournal = require("app/payment_service/models/yandexMoneyJournal");
const YandexMoneyError = require("app/payment_service/errors/YandexMoneyError");

const router = express.Router();

router.get("/", async ({user}, res) => {

    try {
        const acc = await Account.findOne({user_id: user.id}).populate('user_id');
        const {asset_alphanum4, asset_alphanum12, asset_native} =  await acc.balance();
        const profile = acc.user_id;
        return res.send(appSuccessMessage({
            login: profile.login,
            publicKey: acc.publicKey,
            user_info: profile.info(),
            balance: asset_alphanum4,
            cryptoAssets: asset_alphanum12,
            xlm:asset_native
        }));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.post("/trustline/:code", async ({user, params: {code}}, res) => { //{query, params, body}
    try {
        const acc = await Account.findOne({user_id: user.id});
        return res.send(appSuccessMessage(await acc.trustline(code)));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.get("/history/destinations", async ({user, query}, res) => { //{query, params, body}
    const acc = await Account.findOne({user_id: user.id});
    if (!acc.destinations) {
        acc.destinations = new Map();
    }
    const destinations = [];
    acc.destinations.forEach((item) => {
        if (item.name) {
            destinations.push(item);
        }
    });

    destinations.sort((a, b) => {
        if (a.last_operation.created_at > b.last_operation.created_at) {
            return -1;
        }
        if (a.last_operation.created_at < b.last_operation.created_at) {
            return 1;
        }
        // a должно быть равным b
        return 0;
    })

    return res.send(appSuccessMessage({destinations: destinations}));
});

router.get("/history/payments", async ({user, query}, res) => { //{query, params, body}
    try {
        const acc = await Account.findOne({user_id: user.id});
        return res.send(appSuccessMessage(await acc.history(query)));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.post("/payment", async ({body, user}, res) => {
    try {
        const acc = await Account.findOne({user_id: user.id});
        return res.send(appSuccessMessage(await acc.payment(body)));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.post("/payment-path", async ({body, user}, res) => {
    try {
        const acc = await Account.findOne({user_id: user.id});
        return res.send(appSuccessMessage(await acc.paymentPath(body)));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.post("/withdraw", async ({body, user}, res) => {
    try {
        const {to, amount} = body;

        const message=`Перевод в *${to.toString().slice(-4)}`;

        const account = await Account.findOne({user_id: user.id});
        const typeOperation = 'withdraw';
        const model = await YandexMoney.findOne();
        const yandexMoney = model.getPaymentService();
        const transaction = await withdraw({account, amount, currency:"RUB", memo:typeOperation});
        const result = await yandexMoney.requestPayment({to, amount, message});

        if(result["status"] == "success"){
            const processPayment = await yandexMoney.processPayment(result["request_id"]);
            await YandexMoneyJournal.create({
                operation_id: processPayment["payment_id"],
                status: processPayment["status"],
                type: typeOperation,
                stellar_hash: transaction["hash"],
                user_id: user.id,
            })
            return res.send(appSuccessMessage({
                payment_system: yandexMoney.getServiceName(),
                stellar_hash:transaction["hash"],
                status:processPayment["status"],
                operation_id:processPayment["payment_id"],
                to,
                amount,
                credit_amount: processPayment["credit_amount"],
            }));
        }else {
            throw new YandexMoneyError(result,{to, amount, message})
        }

    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

module.exports = router;



