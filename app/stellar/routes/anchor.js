const express = require("express");
const appErrorMessage = require("lib/AppErrorMessage");
const appSuccessMessage = require("lib/AppSuccessMessage");
const Issuer = require("../models/issuer");
const Anchor = require("../models/anchor");
const {mainAnchor} = require("config");
const router = express.Router();


router.post("/create-offer", async ({user, body:{buying, selling , amount, price}}, res) => {
    try {
        const anchor = await Anchor.findOne({stellar_name: mainAnchor});
        return res.send(appSuccessMessage(await anchor.createOffer({buying, amount, price})));
    } catch (e) {
        return res.status(500).send(appErrorMessage(e));
    }
});

router.post("/mint", async ({user, body:{currency, amount}}, res) => {
    try {
        const issuer = await Issuer.findOne({currency});
        const anchor = await Anchor.findOne({stellar_name: mainAnchor});
        return res.send(appSuccessMessage(await issuer.mint({amount, destination: anchor})));
    } catch (e) {
        return res.status(500).send(appErrorMessage(e));
    }
});

router.post("/burn", async ({user, body:{amount},params:{currency}}, res) => {
    try {
        const anchor = await Anchor.findOne({stellar_name: mainAnchor});
        return res.send(appSuccessMessage(await anchor.burn(amount)));
    } catch (e) {
        return res.status(500).send(appErrorMessage(e));
    }
});

module.exports = router;



