'use strict';

const StellarSdk = require("stellar-sdk");
const {BigNumber} = require("bignumber.js");
const config = require("config");
const TransactionError = require("app/stellar/errors/TransactionError");

if(config.usePublicNetwork){
    StellarSdk.Network.usePublicNetwork();
}else{
    StellarSdk.Network.useTestNetwork();
}
const Server = new StellarSdk.Server(config.node);

const submitTransaction = async ({memo = '', operations = [], secretKey}) => {
    try {

        const sourceKeys = StellarSdk.Keypair.fromSecret(secretKey);
        const account = await Server.loadAccount(sourceKeys.publicKey());
        const builder = new StellarSdk.TransactionBuilder(account, {
            memo: StellarSdk.Memo.text(memo),
            fee: StellarSdk.BASE_FEE
        });
        operations.map((item) => {
            builder.addOperation(item);
        });

        const transaction = builder.setTimeout(50).build();
        transaction.sign(sourceKeys);
        const {ledger, hash, result_xdr} = await Server.submitTransaction(transaction);

        return {ledger, hash, result_xdr}; // по аналогии с bridge сервером
    } catch (e) {

        throw new TransactionError(e);
    }
}

const changeTrust = async ({asset, limit = ""}, secretKey=null) => {
    const trust = {asset};
    if (limit) {
        trust.limit = limit.toString();
    }

    const operation = StellarSdk.Operation.changeTrust(trust);

    if(secretKey){
        return submitTransaction({operations: [operation], secretKey});
    }else{
        return operation
    }
}

const createAsset = async ({name, description = ""}, secretKey= null) => {
    const operation = StellarSdk.Operation.manageData({
        name: name,
        value: description,
    });

    if(secretKey){
        return submitTransaction({operations: [operation], secretKey});
    }else{
        return operation
    }
}

const manageOffer = async ({selling, buying, price, amount = 0, offerId = 0}, secretKey= null) => {
    if (selling === buying) {
        throw new Error("can't buy and sell the same asset");
    }
    const operation = StellarSdk.Operation.manageOffer({
        selling: selling,
        buying: buying,
        amount: amount.toString(),
        price: price.toString(),
        offerId: offerId
    });

    if(secretKey){
        return submitTransaction({operations: [operation], secretKey});
    }else{
        return operation
    }
}

const initAccount = async (secretKey) => {
    const pair = StellarSdk.Keypair.random();
    await createWallet({publicKey: pair.publicKey()}, secretKey);

    return {
        secretKey: pair.secret(),
        publicKey: pair.publicKey()
    };
}
/*
const createIssuer = async ({code, description}, anchor) => {
    const {publicKey, secretKey} = await initAccount(anchor.secretKey);
    const asset = await createAsset({name: code, description: description}, secretKey)

    return {publicKey,secretKey,asset}
}

const createWalletByFriendBot = async (destination) => {
    return request({
        uri: 'https://friendbot.stellar.org',
        qs: {addr: destination},
        json: true
    });
}
const payment = async ({from, to, asset, amount, memo = ""}, secretKey = null) => {
    const operation = StellarSdk.Operation.payment({
        destination: to,
        asset: asset,
        amount: amount.toString(),
    });

    if(secretKey){
        return await submitTransaction({operations: [operation], secretKey, memo})
    }else{
        return operation
    }
}
const paymentPath = async ({from, to, sendAsset, sendMax, asset, amount, memo = ""}, secretKey = null) => {
    const operation = StellarSdk.Operation.payment({
        sendAsset: sendAsset,
        sendMax: sendMax.toString(),
        destination: to,
        destAsset: asset,
        destAmount: amount.toString(),
        path: await findAssetPaths({from, to, asset, sendAsset, amount})
    });

    if(secretKey){
        return await submitTransaction({operations: [operation], secretKey, memo})
    }else{
        return operation
    }
}*/

const createWallet = async ({publicKey, balance = '3'}, secretKey) => {
    const operation = StellarSdk.Operation.createAccount({
        destination: publicKey,
        startingBalance: balance // send 3 XLM to new acc
    });

    if(secretKey){
        return submitTransaction({operations: [operation], secretKey});
    }else{
        return operation
    }
}

const issetAsset = async (publicKey, {code, issuer}) => {
    const {balances} = await Server.loadAccount(publicKey);
    const data = balances.filter((item) => {
        const {asset_code, asset_issuer} = item
        return asset_code == code && asset_issuer == issuer;
    });

    return data.length > 0;
}

const findOffer = async ({selling, buying}, publicKey) => {
    let offers = await Server.offers('accounts', publicKey).call();

    while (offers.records.length > 0) {
        const offer = offers.records.find(({selling: sell, buying: buy}) => {
            return sell.asset_code == selling.code &&
                sell.asset_issuer == selling.issuer &&
                buy.asset_code == buying.code &&
                buy.asset_issuer == buying.issuer
        });

        if (offer != null) {
            return offer;
        }
        offers = await offers.next();
    }
    return 0;
}

const getSequenceNumber = async (publicKey) => {
    const acc = await Server.loadAccount(publicKey);
    const {sequence} = acc;
    let number = new BigNumber(sequence);

    return number.toString();
}
const getSequenceNumberNext = async (publicKey) => {
    const {sequence} = await Server.loadAccount(publicKey);
    let number = new BigNumber(sequence);
    number = number.plus(1);

    return number.toString();
}

const isPublicKey = (str) => {
    return StellarSdk.StrKey.isValidEd25519PublicKey(str);
}


const findAssetPaths = async ({from, to, destAsset, sourceAsset, amount}) => {
    const {records} = await Server.paths(from, to, destAsset, amount).call();

    return records.find(({source_asset_code, destination_asset_code}) => {
        return source_asset_code == sourceAsset.code && destination_asset_code == destAsset.code
    }) || {path: []};
}

const getStellarNameFromAddress = function (address = "") {
    const [stellar_name] = address.split("*");
    return stellar_name;
}

module.exports = {
    Server,
    initAccount,
    changeTrust,
    createAsset,
    issetAsset,
    manageOffer,
    findOffer,
    isPublicKey,
    getSequenceNumber,
    getSequenceNumberNext,
    findAssetPaths,
    submitTransaction,
    createWallet,
    getStellarNameFromAddress
};