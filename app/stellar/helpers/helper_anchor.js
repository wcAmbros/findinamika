const {changeTrust} = require("./helper_stellar");
const Anchor = require("../models/anchor");
const Issuer = require('../models/issuer')
const BridgeService = require("../lib/BridgeService")
const {mainAnchor} = require("config");

const deposit = async function ({account, currency = "RUB", amount, memo = "deposit"}) {
    const anchor = await Anchor.findOne({stellar_name: mainAnchor});
    const issuer = await Issuer.findOne({currency});
    await changeTrust({asset:issuer.getAsset()}, account.secretKey);
    const bridgeService = new BridgeService()

    return bridgeService.payment({
        sender: anchor,
        destination: account,
        amount: amount,
        asset: issuer.getAsset(),
        memo: memo
    })
}
const withdraw = async function ({account, currency, amount, memo = "withdraw"}) {
    const anchor = await Anchor.findOne({stellar_name: mainAnchor});
    const issuer = await Issuer.findOne({currency});
    const bridgeService = new BridgeService();

    return bridgeService.payment({
        sender: account,
        destination: anchor,
        amount: amount,
        asset: issuer.getAsset(),
        memo: memo
    })
}

module.exports = {deposit, withdraw}