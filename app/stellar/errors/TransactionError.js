module.exports = class TransactionError extends Error {
    constructor (e) {
        super(e.message);
        this.data = e["response"]["data"];
        this.name = 'TransactionError';
        Error.captureStackTrace(this, this.constructor);
    }
};