const request = require("request-promise");
const {bridge} = require("config");
const {findAssetPaths, getSequenceNumber, getSequenceNumberNext} = require("../helpers/helper_stellar");

class BridgeService{
    /**
     * @see {AccountSchema|IssuerSchema|AnchorSchema} for sender / destination
     *
     * @param {{
     * sender: {publicKey: string, stellar_address: string, secretKey: string },
     * destination: {publicKey: string, stellar_address: string},
     * amount: number,
     * asset: Asset,
     * memo: string
     * }} data
     */
    async payment (data){
        const {sender, destination, amount, asset, memo} = data

        return paymentBridge({
            id: await getSequenceNumber(sender.publicKey),
            amount: amount,
            asset_code: asset.code,
            asset_issuer: asset.issuer,
            sender: sender.stellar_address,
            destination: destination.stellar_address,
            extra_memo: memo,
            use_compliance: true,
            source: sender.secretKey,
        })
    }

    /**
     * @param {{
     * sender: {publicKey: string, stellar_address: string, secretKey: string },
     * destination: {publicKey: string, stellar_address: string},
     * amount: number,
     * asset: Asset,
     * send_asset: Asset,
     * send_max: number
     * memo: string
     * }} data
     */
    async paymentPath (data){
        const {sender, destination, amount, asset, send_asset, send_max, memo} = data

        const {path} = await findAssetPaths({
            from: sender.publicKey,
            to: destination.publicKey,
            amount: amount,
            destAsset: asset,
            sourceAsset: send_asset
        });

        return paymentBridge({
            id: await getSequenceNumberNext(sender.publicKey),
            destination: destination.stellar_address,
            amount: amount,
            asset_code: asset.code,
            asset_issuer: asset.issuer,
            send_asset_code: send_asset.code,
            send_asset_issuer: send_asset.issuer,
            send_max: send_max,
            path: path,
            //extra_memo: memo,
            //use_compliance: true,
            source: sender.secretKey,
        })
    }
}

module.exports = BridgeService

const paymentBridge = async (data) => {

    return request.post({
        url: `${bridge}/payment`,
        form: data,
        json: true
    });
}