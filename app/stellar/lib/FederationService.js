const request = require("request-promise");
const {federation} = require("config");

class FederationService{
    async add (account){
        return request.post({
            url: `${federation}/api/account/save`,
            form: {
                account_id: account.publicKey,
                memo: account.stellar_name,
            },
            json: true
        })
    }
}

module.exports = FederationService