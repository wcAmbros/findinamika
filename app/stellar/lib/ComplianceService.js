const request = require("request-promise");
const {compliance} = require("config");

class ComplianceService {

    async getTransaction(memo){
        return request({
            uri: `${compliance}/api/transaction`,
            memo,
            json: true
        })
    }
}

module.exports = ComplianceService