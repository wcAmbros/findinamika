module.exports = (record) => {
    if (record.type == "create_account") {
        const {id, type, created_at, transaction_hash, funder} = record;
        return {id, type, created_at, transaction_hash, funder}
    }
    if (record.type == "payment") {
        const {id, type, created_at, transaction_hash, asset_code, asset_issuer, from, to, amount} = record;
        return {id, type, created_at, transaction_hash, asset_code, asset_issuer, from, to, amount}
    }
    if (record.type == "path_payment") {
        const {id, type, created_at, transaction_hash, asset_code, asset_issuer, from, to, amount,
            source_amount, source_asset_code, source_asset_issuer} = record

        return {id, type, created_at, transaction_hash, asset_code, asset_issuer, from, to, amount,
            source_amount, source_asset_code, source_asset_issuer}
    }

    return record;
}