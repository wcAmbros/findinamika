const express = require("express");
const Label = require("app/marketplace/models/label");
const Service = require("app/marketplace/models/service");
const {serviceToJSON} = require("app/marketplace/helpers/helper_service");

const appErrorMessage = require("lib/AppErrorMessage");
const appSuccessMessage = require("lib/AppSuccessMessage");

const router = express.Router();

router.get("/", async ({query: {search="", page = 1, limit = 50}}, res) => {
    page = Number.parseInt(page);
    --page;
    limit = Number.parseInt(limit);
    const regex = new RegExp(search, 'i');

    try {
        const list = await Service.find({name: {$regex: regex}})
            .populate('user_id')
            .populate('label_id').skip(page * limit).limit(limit);

        const services = list.map(serviceToJSON);

        const labels = await Label.find({name: {$regex: regex}}).skip(page * limit).limit(limit);

        return res.send(appSuccessMessage({data: labels.concat(services)}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

module.exports = router;



