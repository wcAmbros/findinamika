const express = require("express");
const Order = require("app/marketplace/models/order");
const appErrorMessage = require("lib/AppErrorMessage");
const appSuccessMessage = require("lib/AppSuccessMessage");
const {orderToJSON} = require("../helpers/helper_order");

const router = express.Router();

router.get("/orders", async ({user, query: {page = 1, limit = 50}}, res) => {
    page = Number.parseInt(page);
    limit = Number.parseInt(limit);
    try {
        const list = await Order.find({seller_id: user.id}).populate('seller_id').populate('items')
            .skip((--page) * limit).limit(limit);
        const orders = list.map(orderToJSON);
        return res.send(appSuccessMessage({orders}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});


module.exports = router;



