const express = require("express");
const Label = require("app/marketplace/models/label");
const multer = require("multer");
const storage = require("app/profile/helpers/uploads");
const appErrorMessage = require("lib/AppErrorMessage");
const appSuccessMessage = require("lib/AppSuccessMessage");

const passportJWT = require("app/profile/middleware/passportJWT");

const upload = multer({storage}).single('image');


const router = express.Router();

router.get("/", async ({query: {page = 1, limit = 50}}, res) => {
    page = Number.parseInt(page);
    limit = Number.parseInt(limit);

    try {
        const labels = await Label.find().skip((--page) * limit).limit(limit);

        return res.send(appSuccessMessage({labels}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.get("/items", passportJWT, async ({user, query: {page = 1, limit = 50}}, res) => {
    page = Number.parseInt(page);
    limit = Number.parseInt(limit);

    try {
        const labels = await Label.find({user_id: user.id}).skip((--page) * limit).limit(limit);

        return res.send(appSuccessMessage({labels}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.post("/items/", passportJWT, upload, async ({user, body: {name, description}, file}, res) => {

    try {
        const label = await Label.create(
            {
                name: name,
                description: description,
                user_id: user.id,
                image: file,
            }
        );
        return res.send(appSuccessMessage({label}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.post("/items/:id", passportJWT, upload, async ({user, body, file, params: {id}}, res) => {
    try {
        let data = body;
        if (file) {
            data.image = file;
        }
        const result = await Label.updateOne({user_id: user.id, _id: id}, {$set: data});
        return res.send(appSuccessMessage({result}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.delete("/items/:id", passportJWT, async ({user, params: {id}}, res) => {
    try {
        const result = await Label.deleteOne({user_id: user.id, _id: id});
        return res.send(appSuccessMessage({result}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

module.exports = router;



