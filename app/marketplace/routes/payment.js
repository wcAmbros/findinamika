const express = require("express");
const Order = require("app/marketplace/models/order");
const Service = require("app/marketplace/models/service");
const Account = require("app/stellar/models/account");
const {isPayedOrder} = require("app/marketplace/helpers/helper_order");
const appErrorMessage = require("lib/AppErrorMessage");
const appSuccessMessage = require("lib/AppSuccessMessage");


const router = express.Router();

router.post("/", async ({user, body: {id}}, res) => {
    try {
        await isPayedOrder(id);
        const order = await Order.findOne({_id: id});
        const buyer = await Account.findOne({user_id: user.id});
        const seller = await Account.findOne({user_id: order.seller_id}).populate('user_id');

        const transaction = await buyer.payment({
            to: seller.user_id.login,
            currency: order.currency,
            amount: order.price,
            memo: order.description
        });

        await Order.updateOne({_id: order._id}, {
            $set: {
                payed: true,
                payed_at: Date.now(),
                updated_at: Date.now(),
                transaction: transaction,
                buyer_id: user.id,
            }
        });

        return res.send(appSuccessMessage({order: await Order.findOne({_id: id})}));
    } catch (e) {

        return res.send(appErrorMessage(e));
    }
});

router.post("/service", async ({user, body: {id}}, res) => {

    try {
        const service = await Service.findOne({_id: id});
        const buyer = await Account.findOne({user_id: user.id});
        const seller = await Account.findOne({user_id: service.user_id}).populate('user_id');

        const transaction = await buyer.payment({
            to: seller.user_id.login,
            currency: service.currency,
            amount: service.price,
            memo: service.description
        });

        const order = await Order.create({
            price: service.price,
            currency: service.currency,
            description: service.description,
            payed: true,
            payed_at: Date.now(),
            transaction: transaction,
            items: [service._id],
            seller_id: service.user_id,
            buyer_id: user.id,
        });
        return res.send(appSuccessMessage({order}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});


module.exports = router;
