const express = require("express");
const Favorites = require("../models/favorites");
const {favoritesToJSON} = require("../helpers/helper_favorites");
const appErrorMessage = require("lib/AppErrorMessage");
const appSuccessMessage = require("lib/AppSuccessMessage");

const router = express.Router();

router.get("/items", async ({user, query: {page = 1, limit = 50}}, res) => {
    page = Number.parseInt(page);
    limit = Number.parseInt(limit);

    try {
        const list = await Favorites.find({user_id: user.id}).populate('model_id').skip((--page) * limit).limit(limit);

        const favorites = list.map(favoritesToJSON);
        return res.send(appSuccessMessage({favorites}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.post("/items", async ({user, body: {model_id, type}}, res) => {

    try {
        const data = await  Favorites.findOne({ user_id: user.id, model_id: model_id});
        if(data){
            throw new Error('Item already added');
        }
        const favorite = await Favorites.create({
            user_id: user.id,
            model_id: model_id,
            model_type: type
        });
        return res.send(appSuccessMessage({favorite}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.delete("/items/:id", async ({user, params: {id}}, res) => {
    try {
        const result = await Favorites.deleteOne({user_id: user.id, _id: id});
        return res.send(appSuccessMessage({result}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

module.exports = router;



