const express = require("express");
const Order = require("app/marketplace/models/order");
const {isPayedOrder} = require("app/marketplace/helpers/helper_order");
const appErrorMessage = require("lib/AppErrorMessage");
const appSuccessMessage = require("lib/AppSuccessMessage");

const router = express.Router();

/* Когда  продавец формирует заказ */

router.post("/", async ({user, body}, res) => {

    try {
        const model = new Order();
        const order = await Order.create(model.makeData(body, user));
        return res.send(appSuccessMessage({order}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.put("/:id", async ({user, query, params: {id}}, res) => {
    try {
        await isPayedOrder(id);
        const model = new Order();
        const result = await Order.updateOne({seller_id: user.id, _id: id}, {
            $set: model.makeData(query, user)
        });
        return res.send(appSuccessMessage({result}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.delete("/:id", async ({user, params: {id}}, res) => {
    try {
        await isPayedOrder(id);
        const result = await Order.deleteOne({seller_id: user.id, _id: id});
        return res.send(appSuccessMessage({result}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});


module.exports = router;



