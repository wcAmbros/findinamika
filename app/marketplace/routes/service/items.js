const express = require("express");
const Service = require("app/marketplace/models/service");
const appErrorMessage = require("lib/AppErrorMessage");
const appSuccessMessage = require("lib/AppSuccessMessage");
const {serviceToJSON} = require("app/marketplace/helpers/helper_service");

const multer = require("multer");
const storage = require("app/profile/helpers/uploads");
const upload = multer({storage}).single('image');


const router = express.Router();

router.get("/", async ({user, query: {page = 1, limit = 50}}, res) => {
    page = Number.parseInt(page);
    limit = Number.parseInt(limit);

    try {
        const list = await Service.find({user_id: user.id})
            .populate('user_id')
            .populate('label_id')
            .skip((--page) * limit)
            .limit(limit);
        const services = list.map(serviceToJSON);
        return res.send(appSuccessMessage({services}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.post("/", upload, async ({user, file, body: {name, price, currency = "RUB", description, label_id, space, measure}}, res) => {

    try {
        const service = await Service.create(
            {
                name: name,
                price: Number.parseFloat(price).toFixed(2),
                currency: currency,
                description: description,
                user_id: user.id,
                label_id: label_id,
                space: space,
                measure: measure,
                image: file
            }
        );
        return res.send(appSuccessMessage({service}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.post("/:id", upload, async ({user, body, file, params: {id}}, res) => {
    try {
        if (file) {
            body.image = file;
        }
        const result = await Service.updateOne({user_id: user.id, _id: id}, {$set: body});
        return res.send(appSuccessMessage({result}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.delete("/:id", async ({user, params: {id}}, res) => {
    try {
        const result = await Service.deleteOne({user_id: user.id, _id: id});
        return res.send(appSuccessMessage({result}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

module.exports = router;



