const express = require("express");
const Order = require("../models/order");
const Service = require("../models/service");
const appErrorMessage = require("lib/AppErrorMessage");
const appSuccessMessage = require("lib/AppSuccessMessage");
const passportJWT = require("app/profile/middleware/passportJWT");
const {orderToJSON} = require("app/marketplace/helpers/helper_order");
const User = require("app/profile/models/user");

const router = express.Router();

router.use("/items", passportJWT, require("./order/items"));

router.get("/", async ({query: {id, page = 1, limit = 50}}, res) => {
    page = Number.parseInt(page);
    limit = Number.parseInt(limit);

    const filter = (id) ? {_id: id} : {};

    try {
        const list = await Order.find(filter).populate('seller_id').populate('items')
            .skip((--page) * limit).limit(limit);
        const orders = list.map(orderToJSON);
        return res.send(appSuccessMessage({orders}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.post("/send", passportJWT, async ({user, body , wss}, res) => {

    try {
        const model = new Order();
        const raw = await Order.create(model.makeData(body, user));
        const order = await Order.findOne({_id: raw._id}).populate('seller_id').populate('items');
        order.items.forEach(async (item) => {
            await Service.populate(item, {"path": "label_id"});
            item.label = (item.label_id) ? item.label_id.toJSON():{};
            delete item.label._id;
        });

        const profile = await User.findOne({login: body.to});
        const seller = await User.findOne({_id: user.id});
        const data = {
            type: 'order',
            message: `send order from seller: ${seller.login}`,
            data: orderToJSON(order),
        };
        await profile.addNotice(data);
        //wss.sendNotice(user, data);
        return res.send(appSuccessMessage({order}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});


module.exports = router;



