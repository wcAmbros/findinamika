const express = require("express");
const Service = require("../models/service");
const {serviceToJSON} = require("app/marketplace/helpers/helper_service");
const appErrorMessage = require("lib/AppErrorMessage");
const appSuccessMessage = require("lib/AppSuccessMessage");
const passportJWT = require("app/profile/middleware/passportJWT");
const router = express.Router();

router.get("/", async ({query: {id, page = 1, limit = 50}}, res) => {
    const filter = (id) ? {_id: id} : {};
    try {
        const services = await findServices(filter, page, limit);
        return res.send(appSuccessMessage({services}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.get("/search", async ({query: {search, page = 1, limit = 50}}, res) => {
    page = Number.parseInt(page);
    limit = Number.parseInt(limit);
    try {
        const services = await Service.find({$text: {$search: search}}).skip((--page) * limit).limit(limit);

        return res.send(appSuccessMessage({services}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.get("/spaces", async ({query: {space, page = 1, limit = 50}}, res) => {
    try {
        const services = await findServices({space}, page, limit);
        return res.send(appSuccessMessage({services}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.get("/labels", async ({query: {label, page = 1, limit = 50}}, res) => {
    try {
        const services = await findServicesByLabel({label}, page, limit);
        return res.send(appSuccessMessage({services}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});


router.use("/items", passportJWT, require("./service/items"));

module.exports = router;

async function findServices(filter, page = 1, limit = 50) {
    page = Number.parseInt(page);
    limit = Number.parseInt(limit);
    page = (page > 1) ? page : 1;
    limit = (limit > 1) ? limit : 50;
    const list = await Service.find(filter)
        .populate('user_id')
        .populate('label_id').skip((--page) * limit).limit(limit);
    return list.map(serviceToJSON);
}


async function findServicesByLabel({label=""}, page = 1, limit = 50) {
    page = Number.parseInt(page);
    limit = Number.parseInt(limit);
    page = (page > 1) ? page - 1 : 0;
    limit = (limit > 1) ? limit : 50;

    let list = await Service.find()
        .populate('user_id')
        .populate({
            path:'label_id',
            match:{name:{ $regex: label, $options: 'i'}}
        })

    if(label){
        list = list.filter(i=>i.label_id);
    }
    return list.slice(page, page + limit).map(serviceToJSON);
}

