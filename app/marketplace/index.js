const express = require("express");
const passportJWT = require("app/profile/middleware/passportJWT");

const router = express.Router();

router.use("/buyer", passportJWT, require("./routes/buyer"));
router.use("/favorites", passportJWT, require("./routes/favorites"));
router.use("/label", require("./routes/label"));
router.use("/order", require("./routes/order"));
router.use("/payment", passportJWT, require("./routes/payment"));
router.use("/seller", passportJWT, require("./routes/seller"));
router.use("/service", require("./routes/service"));

router.use("/search", require("./routes/search"));

module.exports = router;
