const Order = require("../models/order");

const isPayedOrder = async (id) => {
    const order = await Order.findOne({_id: id});
    if (order.payed) {
        throw new Error("can't modify or delete payed order");
    }
}

const orderToJSON = (item) => {
    const newItem = item.toJSON();
    newItem.seller = {};
    if(typeof item.seller_id == "object"){
        newItem.seller = item.seller_id.info();
        delete newItem.seller_id;
    }

    return newItem;
}

module.exports = {isPayedOrder, orderToJSON}