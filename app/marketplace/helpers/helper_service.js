const serviceToJSON = (item) => {
    const newItem = item.toJSON();
    newItem.seller = {};
    newItem.label = {};

    if (isModel(item.user_id)) {
        newItem.seller = item.user_id.info();
        delete newItem.user_id;
    }

    if (isModel(item.label_id)) {
        newItem.label = item.label_id.toJSON();
        delete newItem.label_id;
    }

    return newItem;
}


function isModel(obj) {
    return obj !== null && typeof obj === 'object' && typeof obj.constructor === 'function';
}


module.exports = {serviceToJSON}