const {serviceToJSON} = require('./helper_service');

const favoritesToJSON = (item) => {
    const newItem = item.toJSON();
    newItem.data = {};
    delete newItem.user_id;

    if (isModel(item.model_id) && item.model_type == 'Label') {
        newItem.data = item.model_id.toJSON();
        delete newItem.model_id;
    }
    if (isModel(item.model_id) && item.model_type == 'Service') {
        newItem.data = serviceToJSON(item.model_id);
        delete newItem.model_id;
    }

    return newItem;
}


function isModel(obj) {
    return obj !== null && typeof obj === 'object' && typeof obj.constructor === 'function';
}

module.exports = {favoritesToJSON}