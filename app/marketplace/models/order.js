const mongoose = require("mongoose");
const autoIncrement = require("mongoose-auto-increment");

const Schema = mongoose.Schema;

const OrderSchema = new Schema(
    {
        number: {type: Number},
        price: {type: Number},
        name: {type: String},
        currency: {type: String},
        description: {type: String},

        payed: {type: Boolean, default:false},
        payed_at: {type: Date},
        transaction: {
            hash: {type: String},
            ledger: {type: String}
        },
        items: [{type: Schema.Types.ObjectId, ref: 'Service'}],
        seller_id: {type: Schema.Types.ObjectId, ref: 'User'},
        buyer_id: {type: Schema.Types.ObjectId, ref: 'User'},
        created_at: {type: Date, default: Date.now},
        updated_at: {type: Date, default: Date.now},
    }
)

OrderSchema.methods.makeData = ({id, name, price, currency = "RUB", description}, user) => {
    price = Number.parseFloat(price);
    return {
        name: name,
        price: price,
        currency: currency,
        description: description,
        items: [id],
        seller_id: user.id,
        updated_at: Date.now(),
    }
}

OrderSchema.set('toJSON', {
        virtuals: true,
        versionKey: false,
        transform: (doc, ret) => {
            delete ret._id;
            delete ret.buyer_notice;
            delete ret.seller_notice;
        }
    }
);

OrderSchema.plugin(autoIncrement.plugin, {model: 'Order', field: 'number',startAt: 1,});

module.exports = mongoose.model("Order", OrderSchema);
