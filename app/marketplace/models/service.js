const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const ServiceSchema = new Schema(
    {
        name: {type: String},
        user_id: {type: Schema.Types.ObjectId, ref: 'User'},
        label_id: {type: Schema.Types.ObjectId, ref: 'Label'},
        space: {type: String},
        measure: {type: String},
        price: {type: Number},
        description: {type: String},
        currency: {type: String, default: "RUB"},
        timestamp: {type: Date, default: Date.now},
        image: {filename: String, mimetype: {type: String, default: "image/jpg"}},
    }
)

ServiceSchema.virtual('obj_type').get(function () {
        return 'Service';
});

ServiceSchema.set('toJSON', {
        virtuals: true,
        versionKey:false,
        transform: (doc, ret)=>{delete ret._id}});

ServiceSchema.index({name: 'text', space: 'text'});


module.exports = mongoose.model("Service", ServiceSchema);