const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const LabelSchema = new Schema(
    {
        user_id: {type: Schema.Types.ObjectId, ref: 'User'},
        name: {type: String},
        description: {type: String},
        image: {filename: String, mimetype: {type: String, default: "image/jpg"}},
    }
)

LabelSchema.virtual('obj_type').get(function () {
        return 'Label';
});

LabelSchema.set('toJSON', {
        virtuals: true,
        versionKey:false,
        transform: (doc, ret)=>{delete ret._id}});

LabelSchema.index({name: 'text'});

module.exports = mongoose.model("Label", LabelSchema);