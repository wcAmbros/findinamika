const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const FavoritesSchema = new Schema(
    {
        user_id: {
            type: Schema.Types.ObjectId, ref: 'User',
            required: true
        },
        model_id: {
            type: Schema.Types.ObjectId, refPath: 'model_type',
            required: true
        },
        model_type: {
            type: String,
            required: true,
            enum: ['Label', 'Service']
        }
    }
)


FavoritesSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: (doc, ret) => {
        delete ret._id
    }
});

FavoritesSchema.post('find', async function (models) {
    for (let model of models) {
        if (model.model_type == 'Service' && isModel(model.model_id)) {
            await model.model_id.populate('user_id')
                .populate('label_id')
                .execPopulate();
        }
    }
});

function isModel(obj) {
    return obj !== null && typeof obj === 'object' && typeof obj.constructor === 'function';
}

module.exports = mongoose.model("Favorites", FavoritesSchema);