const express = require("express");


const router = express.Router();

router.use("/vk", require("./routes/vk"));
router.use("/tochka", require("./routes/tochka"));
module.exports = router;