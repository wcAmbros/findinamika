const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const clientSchema = new Schema(
    {
        user_id: {
            type: Schema.Types.ObjectId, ref: 'User',
            required: true
        },
        clientSecret: {
            type: String,
            required: true
        }
    }
);

module.exports = mongoose.model("Client", clientSchema);