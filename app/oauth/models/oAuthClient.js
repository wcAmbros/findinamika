const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const OAuthClientSchema = new Schema(
    {
        user_id: {
            type: Schema.Types.ObjectId, ref: 'User',
            required: true
        },
        accessToken: String,
        refreshToken: String,
        expiresIn: Number,
        clientID: Number, // user id in oauth 2.0 service
        provider: String, //vk, google, git etc...
        username: String, //id4654651, etc...
    }
);

module.exports = mongoose.model("OAuthClient", OAuthClientSchema);