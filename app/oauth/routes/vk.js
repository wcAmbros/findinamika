const express = require("express");
const passport = require("passport");
const {Strategy: VKontakteStrategy} = require("passport-vkontakte");

const OAuthClient = require("app/oauth/models/oAuthClient");
const passportJWT = require("app/profile/middleware/passportJWT");

const {randomString} = require("../helpers/helpers_auth");
const {createUser, authorizeUser} = require("app/profile/helpers/helpers_user");

const appErrorMessage = require("lib/AppErrorMessage");
const appSuccessMessage = require("lib/AppSuccessMessage");
const nodeCache = require("node-cache");
const cache = new nodeCache({stdTTL: 120, checkperiod: 120});
const crypto = require('crypto');

passport.use(new VKontakteStrategy({
        clientID: 6900606,
        clientSecret: 'GeccIfOgKwJkF6m9qGUc'
    },
    async (accessToken, refreshToken, params, profile, done) => {
        return done(null, {id: 0, vk: {params, profile}});
    }
));

const router = express.Router();

router.get('/', authVk,
    async function ({user}, res) {
        try {
            const {vk: {profile, params}} = user;

            const client = await OAuthClient.findOne({
                clientID: params.user_id,
                provider: 'vkontakte'
            }).populate('user_id');
            if (client) {
                return res.send(authorizeUser(client.user_id));
            } else {
                const newUser = await createUserByVK({profile, params});
                return res.send(authorizeUser(newUser));
            }
        } catch (e) {
            return res.send(appErrorMessage(e));
        }
    });

router.get('/link', async function (req, res) {

    const cryptoHash = crypto.createHash('sha256');
    cryptoHash.update(JSON.stringify(req.query));

    const hash = cryptoHash.digest('hex');
    cache.set(hash, req.query);

    return res.redirect(`link/${hash}`);
});

router.get('/unlink', passportJWT, async function ({user}, res) {
    const result = await OAuthClient.deleteOne({user_id: user.id});
    res.send(appSuccessMessage(result));
});

router.get('/link/:hash', setBearerToken, passportJWT, authVk, async function ({user, jwtUser}, res) {
    try {

        const {vk: {profile, params}} = user;
        await linkUserVK({profile, params, jwtUser});

        return res.send(appSuccessMessage({message: 'successfull link'}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});


router.post("/isset", passportJWT, async ({body: {list_id = []}}, res) => {
    try {
        const result = [];
        const data = await OAuthClient.find({username: list_id, provider: 'vkontakte'}).populate('user_id');

        const clients = {};
        data.map((item) => {
            clients[item.username] = item;
        })

        list_id.map((item) => {
            if (clients[item]) {
                const user = clients[item].user_id;
                result.push({
                    isset: true,
                    id: item,
                    login: user.login
                });
            } else {
                result.push({
                    isset: false,
                    id: item
                });
            }
        })

        return res.send(appSuccessMessage({result}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});


module.exports = router;

function setBearerToken(req, res, next) {
    const {access_token} = cache.get(req.params.hash);
    req.headers.authorization = `Bearer ${access_token}`;
    next();
}

function authVk(req, res, next) {
    if (req.user) {
        req.jwtUser = req.user;
    }

    return passport.authenticate('vkontakte', {
            response_type: 'code',
            apiVersion: '5.101',
            callbackURL: `https://${req.get('host')}${req.baseUrl}${req.path}`
        }
    )(req, res, next)
}


async function createUserByVK({profile, params}) {
    const user = await createUser({
        login: profile.username,
        password: randomString(7)
    });
    await OAuthClient.create({
            user_id: user._id,
            accessToken: params.access_token,
            expiresIn: params.expires_in,
            clientID: params.user_id,
            provider: 'vkontakte',
            username: profile.username,
        }
    );
    const data = profile._json;
    // user.avatar = {
    //     filename: data.photo,
    //     mimeType: 'jpeg',
    // };

    user.first_name = data.first_name;
    user.last_name = data.last_name;

    await user.save();

    return user;
}

async function linkUserVK({profile, params, jwtUser}) {

    const client = await OAuthClient.findOne({clientID: params.user_id});

    if (client) {
        throw new Error('service account already linked to user');
    } else {
        await OAuthClient.create({
                user_id: jwtUser.id,
                accessToken: params.access_token,
                expiresIn: params.expires_in,
                clientID: params.user_id,
                provider: 'vkontakte',
                username: profile.username,
            }
        );
    }
}
