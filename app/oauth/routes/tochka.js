const express = require("express");
const passport = require("passport");
const {Strategy: VKontakteStrategy} = require("passport-vkontakte");

const OAuthClient = require("app/oauth/models/oAuthClient");
const passportJWT = require("app/profile/middleware/passportJWT");

const {randomString} = require("../helpers/helpers_auth");
const {createUser, authorizeUser} = require("app/profile/helpers/helpers_user");

const appErrorMessage = require("lib/AppErrorMessage");
const appSuccessMessage = require("lib/AppSuccessMessage");
const nodeCache = require("node-cache");
const cache = new nodeCache({stdTTL: 120, checkperiod: 120});
const crypto = require('crypto');
/*
{
    clientID: 'niPC86mshlj43OSFWvChE6gbl2J6u1fu',
    clientSecret: 'SjrLesHxcL84UIaJ9xmDLzmlgQw3SHjP'
    redirectURL: 'https://api.findinamika.com/api/oauth/tochka/'
}
*/

const router = express.Router();

router.get('/',
    async function ({query, body}, res) {
        try {
            return res.send({query, body});

        } catch (e) {
            return res.send(appErrorMessage(e));
        }
    });



module.exports = router;