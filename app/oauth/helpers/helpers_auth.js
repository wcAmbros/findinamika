const jwt = require("jsonwebtoken");
const conf = require("config");

function token(user) {
    return jwt.sign({
        id: user._id,
        role: user.role,
    }, conf.token.secret, {expiresIn: conf.token.expiresIn})
}

function generateLoginPwd({prefix}) {
    const date = new Date().getTime();
    const login = `${prefix}_${date.toString().slice(0, 12)}`;
    return {
        login,
        password: randomString(16)
    }
}

function randomString(length = 7) {
    let result = '';
    const characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz~!@-#$';

    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function createVerifyCode(length = 7) {
    let result = '';
    const characters = '0123456789';

    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function createVerifyData(user) {
    const data = {
        id: user._id,
        login: user.login,
        email_address: user.email_address,
        verifyCode: createVerifyCode(),
        expires: 60 * 3, //3 minutes
    };

    const resetToken = jwt.sign(data, conf.token.secret, {expiresIn: 60 * 3}) //3 minutes

    data.resetToken = resetToken;
    return data;
}

async function checkVerifyDataByToken(token) {
    return await jwt.verify(token, conf.token.secret);
}

module.exports = {token, generateLoginPwd, randomString, createVerifyData, checkVerifyDataByToken};