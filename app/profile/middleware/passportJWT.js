const passport = require("passport");
const {Strategy: JwtStrategy, ExtractJwt} = require("passport-jwt");

const config = require("config");
const jwtBlacklist = require("lib/jwtBlackList");
const appErrorMessage = require("lib/AppErrorMessage");

const tokenSecret = config.token.secret;

passport.use(new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromExtractors([
        ExtractJwt.fromAuthHeaderAsBearerToken(),
        ExtractJwt.fromUrlQueryParameter('access_token')
    ]),
    secretOrKey: tokenSecret,
}, (token, done) => done(null, token)));

module.exports = async (req, res, next) => {

    try {

        if(req.headers.authorization){
            await jwtBlacklist.verify(req.headers.authorization);
        }else if(req.query.access_token){
            await jwtBlacklist.verify(`Bearer ${req.query.access_token}`);
        }else{
            throw new Error("Failed: header 'Authorization' or param 'access_token' is empty");
        }

    }catch (e) {
        return res.send(appErrorMessage(e));
    }

    return passport.authenticate('jwt', {session: false}, async (err, user) => {
        if (!user) {
            return res.status(401).send({success: false, message: 'Failed to authenticate token.'})
        } else {
            req.login(user, (err) => {
                return (err) ? next(err) : next();
            });
        }

    })(req, res, next);
};
