const express = require("express");
const Friends = require("../models/friends");
const User = require("../models/user");
const appErrorMessage = require("lib/AppErrorMessage");
const appSuccessMessage = require("lib/AppSuccessMessage");


const router = express.Router();

router.get("/", async ({user, query: {id, page = 1, limit = 50}}, res) => {
    page = Number.parseInt(page);
    limit = Number.parseInt(limit);
    const user_id = (id) ? id : user.id;
    try {
        const list = await Friends.find({user_id}).skip((--page) * limit).populate('friend_id').limit(limit);

        const friends = list.map((item) => {
            return item.friend_id.info();
        });
        return res.send(appSuccessMessage({friends}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.post("/add-by-login", async ({user, body: {login}}, res) => {

    try {
        const profile = await User.findOne({login});
        if(!profile){
            throw new Error('user not fount');
        }

        if (await Friends.findOne({friend_id: profile._id, user_id: user.id})) {
            throw new Error("friend is added");
        }
        await Friends.create({friend_id: user.id, user_id: profile._id});
        await Friends.create({friend_id: profile._id, user_id: user.id});
        return res.send(appSuccessMessage());
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.post("/", async ({user, body: {id}}, res) => {

    try {
        const profile = await User.findOne({_id:id});
        if(!profile){
            throw new Error('user not fount');
        }
        if (await Friends.findOne({friend_id: id, user_id: user.id})) {
            throw new Error("friend is added");
        }
        await Friends.create({friend_id: user.id, user_id: id});
        await Friends.create({friend_id: id, user_id: user.id});
        return res.send(appSuccessMessage());
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});


router.delete("/:id", async ({user, params: {id}}, res) => {
    try {
        await Friends.deleteOne({friend_id: user.id, user_id: id});
        const result = await Friends.deleteOne({friend_id: id, user_id: user.id});
        return res.send(appSuccessMessage({result}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

module.exports = router;



