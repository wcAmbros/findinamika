const express = require("express");
const BankDetails = require("app/profile/models/bank_details");
const appErrorMessage = require("lib/AppErrorMessage");
const appSuccessMessage = require("lib/AppSuccessMessage");


const router = express.Router();

router.get("/", async ({user}, res) => {
    try {
        const bank_details = await BankDetails.findOne({user_id: user.id});
        return res.send(appSuccessMessage({bank_details}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.post("/", async ({user, body}, res) => {
    const  {account_number, organization, INN, BIC, KPP, IBAN, SWIFT} = body;
    const data =  {account_number, organization, INN, BIC, KPP, IBAN, SWIFT};
    try {
        let bank_details;
        bank_details = await BankDetails.updateOne({user_id: user.id}, data);
        if(!bank_details.id){
            data.user_id = user.id;
            bank_details = await BankDetails.create(data);
        }

        return res.send(appSuccessMessage({bank_details}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});


router.delete("/", async ({user}, res) => {
    try {
        const result =  await BankDetails.deleteOne({ user_id: user.id});
        return res.send(appSuccessMessage({result}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

module.exports = router;



