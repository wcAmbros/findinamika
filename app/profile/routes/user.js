const express = require("express");
const multer = require("multer");
const User = require("../models/user");
const Notice = require("../models/notice");

const appErrorMessage = require("lib/AppErrorMessage");
const appSuccessMessage = require("lib/AppSuccessMessage");
const jwtBlacklist = require("lib/jwtBlackList");
const passportJWT = require("app/profile/middleware/passportJWT");

const storage = require("../helpers/uploads");
const {validate, createUser, authorizeUser} = require("../helpers/helpers_user");
const {sendNewPassword, sendVerifyCode} = require("../helpers/helpers_sendmail");
const {randomString, createVerifyData, checkVerifyDataByToken} = require("app/oauth/helpers/helpers_auth");
const router = express.Router();

const upload = multer({storage}).fields([
    {name: 'avatar', maxCount: 1},
    {name: 'photo_id_front', maxCount: 1},
    {name: 'photo_id_back', maxCount: 1}
]);

router.post("/login",
    async ({body}, res) => {
        try {
            await validate(body);
            const {login, password} = body;
            const user = await User.findOne({login: login});

            if (user && user.authenticate(password)) {
                return res.send(authorizeUser(user));
            } else {
                throw new Error("Error authorized");
            }

        } catch (e) {
            res.send(appErrorMessage(e));
        }

    }
);

router.get("/logout", passportJWT, async (req, res) => {
    jwtBlacklist.add(req.headers['authorization']);
    req.logout();
    return res.send({success: true, message: "logout"});
});

router.get("/all", async (req, res) => {
    return res.send(await User.find());
});

router.post("/signup",
    async ({body}, res) => {
        try {
            await validate(body);
            const user = await createUser(body);
            return res.send(authorizeUser(user));
        } catch (e) {
            return res.send(appErrorMessage(e));
        }
    }
);

router.post("/update", passportJWT, upload, async ({body, user, files}, res) => {
    try {
        const profile = await User.findOne({_id: user.id});
        if (!profile) {
            throw new Error("can't find profile");
        }

        profile.fillData(body, files);
        await profile.save();

        return res.send(appSuccessMessage({message: "update profile"}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }

});


router.get("/notice", passportJWT, async ({user}, res) => {
    try {
        const notice = await Notice.find({user_id: user.id});
        await Notice.deleteMany({user_id: user.id});

        return res.send(appSuccessMessage({notice}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }

});

router.get("/info/:id", async ({params: {id}}, res) => {
    try {
        const user = await User.findOne({_id: id});
        return res.send(appSuccessMessage({user: user.info()}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});


router.get("/photo/front", passportJWT, async ({user}, res) => {
    const {photo_id_front} = await User.findOne({_id: user.id});
    res.contentType(photo_id_front.mimetype);
    return res.send(photo_id_front.data);
});

router.get("/photo/back", passportJWT, async ({user}, res) => {
    const {photo_id_back} = await User.findOne({_id: user.id});
    res.contentType(photo_id_back.mimetype);
    return res.send(photo_id_back.data);
});


router.post("/reset/verify-code", async ({body: {email_address}}, res) => {
    const user = await User.findOne({email_address});

    try {
        if (!user) {
            throw new Error("can't find profile");
        }
        const verifyData = createVerifyData(user);
        sendVerifyCode(verifyData);

        return res.send(appSuccessMessage({
            resetToken: verifyData.resetToken,
            expires: verifyData.expires
        }));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

router.post("/reset/password", async ({body: {resetToken, verifyCode}}, res) => {

    try {
        const resetUser = await checkVerifyDataByToken(resetToken);
        const user = await User.findOne({_id: resetUser.id});

        if (!user) {
            throw new Error("can't find profile");
        }
        if(resetUser.verifyCode != verifyCode){
            throw new Error("invalid verify code");
        }

        const password = randomString();
        user.setPassword(password);
        await user.save();
        sendNewPassword({user, password});


        return res.send(appSuccessMessage({message: "send new password to email"}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});


router.use("/bank_details", passportJWT, require('./user/bank_details'));
module.exports = router;



