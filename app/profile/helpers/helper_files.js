const fs = require("fs");

function uploadFile(file, oldFile = null, dir = './uploads') {
    removeFile(oldFile, dir);

    return {
        filename: file.filename,
        mimetype: file.mimetype,
    };
}

function uploadFileToBuffer(file, dir = './uploads') {
    const path = getFilePath(file, dir);
    const buffer = fs.readFileSync(path);

    removeFile(file, dir);

    return {
        buffer: buffer,
        mimetype: file.mimetype,
    };

}


function removeFile(file, dir) {
    if (file == null) {
        return null;
    }
    const path = getFilePath(file, dir);
    try {
        if (fs.lstatSync(path).isFile()) {
            fs.unlinkSync(path);
        }
    }catch (e) {
        console.log(e);
        return null;
    }
}

function getFilePath(file, dir) {
    return (file.path) ? `./${file.path}` : `${dir}/${file.filename}`;
}

module.exports = {uploadFile, uploadFileToBuffer}