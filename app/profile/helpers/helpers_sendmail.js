const nodemailer = require("nodemailer");
const transportOpt = {
    host: 'smtp.yandex.ru',
    port: 465,
    secure: true,
    auth: {
        type: 'OAuth2',
        user: 'findinamika@yandex.ru',
        clientId: '880c0cdb983947b6b35a80c322736cf3',
        clientSecret: 'f0dbac3e7307446985c1d66e4b2a7304',
        accessToken: 'AgAAAAAz2ydKAAXU8xo_TJfKfULyrFBDk7aMdh8'
    }
}

async function sendVerifyCode(verifyUser) {

    const transporter = nodemailer.createTransport(transportOpt);
    const mail = {
        from: 'findinamika@yandex.ru',
        to: verifyUser.email_address,
        subject: 'Reset password - verify code',
        html: `<p>You are receiving this because you (or someone else) 
        have requested the reset of the password for your account <strong>${verifyUser.login}</strong></p>
        <p>Input verify code: <strong>${verifyUser.verifyCode}</strong></p>
        <p>If you did not request this, please ignore this email and your password will remain unchanged</p>`
    }

    return await transporter.sendMail(mail);
}

async function sendNewPassword({user, password}) {
    const transporter = nodemailer.createTransport(transportOpt);
    const mail = {
        from: 'findinamika@yandex.ru',
        to: user.email_address,
        subject: 'New password',
        html: `<p>Your new password: <strong>${password}</strong></p>`
    }

    return await transporter.sendMail(mail);
}

module.exports = {sendNewPassword, sendVerifyCode};