const User = require("../models/user");
const {changeTrust, initAccount} = require("app/stellar/helpers/helper_stellar");
const Account = require("app/stellar/models/account");
const Anchor = require("app/stellar/models/anchor");
const Issuer = require("app/stellar/models/issuer");
const {token} = require("app/oauth/helpers/helpers_auth");
const FederationService = require("app/stellar/lib/FederationService");
const {mainAnchor} = require("config");

async function validate(data) {
    const model = new User(data);
    await model.validate();
}

async function createUser({login, password}) {
    const anchor = await Anchor.findOne({stellar_name: mainAnchor});
    const keys = await anchor.createStellarAccount();
    const user = new User({login: login.trim()});

    user.setPassword(password.trim());
    await user.save();

    const issuer = await Issuer.findOne({currency: "RUB"})
    const acc = await Account.create({
            user_id: user._id,
            secretKey: keys.secretKey,
            publicKey: keys.publicKey,
        }
    );
    await changeTrust({asset:issuer.getAsset()}, acc.secretKey);
    const federationService = new FederationService();
    await federationService.add(acc);

    return user;
}

function authorizeUser(user) {
    if (user) {
        const access_token = token(user);
        return {
            success: true,
            message: "User authorized",
            token: `Bearer ${access_token}`,
            access_token: access_token,
            token_type: 'bearer'
        };
    }

    throw new Error("Error authorized");
}

module.exports = {validate, createUser, authorizeUser}