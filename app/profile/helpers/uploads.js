const multer = require("multer");
const mime = require("mime");
const crypto = require("crypto");

module.exports = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads')
    },
    filename: function (req, file, cb) {
        const nonce = crypto.randomBytes(16);
        const hash = crypto.createHash('sha256');
        const attachment = {
            nonce: nonce.toString('hex'),
            file: file
        };
        hash.update(JSON.stringify(attachment));
        const newName = hash.digest('hex');
        let extension = mime.getExtension(file.mimetype);
        if(file.mimetype == 'image/jpg'){
            extension = 'jpg';
        }

        cb(null, `${newName}.${extension}`);
    }
});