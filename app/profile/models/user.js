const mongoose = require("mongoose");
const crypto = require("crypto");
const Notice = require("./notice");
const {uploadFile, uploadFileToBuffer} = require("../helpers/helper_files");
const Schema = mongoose.Schema;
/**
 * @see https://github.com/stellar/stellar-protocol/blob/master/ecosystem/sep-0009.md
 * */
const UserSchema = new Schema(
    {
        login: {type: String, required: true, unique: true},
        password: {type: String, required: true,},
        salt: {type: String},

        /**Поля описанные в спецификации sep-0009**/
        /**public info*/
        first_name: {type: String}, //имя
        last_name: {type: String},// фамилия
        additional_name: {type: String},// отчество
        birth_date: {type: Date}, //1988-02-22
        mobile_number: {type: String}, /** @see https://en.wikipedia.org/wiki/E.164*/
        country: {type: String},
        city: {type: String},
        address: {type: String}, //@]
        email_address: {type: String},

        /**private info*/
        id_type: {type: String}, //passport, etc..
        id_number: {type: String},
        id_issue_date: {type: Date},
        photo_id_front: {buffer: Buffer, mimetype: {type: String, default: "image/jpg"}},
        photo_id_back: {buffer: Buffer, mimetype: {type: String, default: "image/jpg"}},
        /**Конец спецификации sep-0009**/

        /** additional info*/
        about: {type: String},

        role: {type: String, default: 'user'},
        avatar: {filename: String, mimetype: {type: String, default: "image/jpg"}},

        /**options info*/
        allow_geolocation: {type: Boolean, default: false},
        allow_notice: {type: Boolean, default: true},
        allow_personal_data: {type: Boolean, default: false},
    },
    {
        strict: false,
        virtuals: true
    }
)

UserSchema.methods.encryptPassword = function (password) {
    const hash = crypto.createHmac('sha256', this.salt)
        .update(password)
        .digest('hex');

    return hash;
};

UserSchema.methods.makeSalt = () => (Math.round((new Date().valueOf() * Math.random())));

UserSchema.methods.setPassword = function (password) {
    this.salt = this.makeSalt();
    this.password = this.encryptPassword(password);
};

UserSchema.methods.authenticate = function (plainText) {
    return this.encryptPassword(plainText) === this.password;
}

UserSchema.methods.addNotice = async function ({type, message, data}) {
    if (this.allow_notice) {

        await Notice.create({
            type, message, data,
            user_id: this._id
        });
        return true;
    }
    return false;
}


UserSchema.methods.fillData = function (data = {}, files = []) {
    for (let key in data) {
        this[key] = data[key].trim();
    }

    if (data.password) {
        this.setPassword(data.password.trim());
    }

    if (files.avatar) {
        this.avatar = uploadFile(files.avatar[0], this.avatar);
    }

    if (files.photo_id_front) {
        this.photo_id_front = uploadFileToBuffer(files.photo_id_front[0]);
    }

    if (files.photo_id_back) {
        this.photo_id_back = uploadFileToBuffer(files.photo_id_back[0]);
    }
}

UserSchema.methods.info = function () {
    return {
        id: this._id,
        login: this.login,
        about: this.about,
        first_name: this.first_name,
        last_name: this.last_name,
        additional_name: this.additional_name,
        birth_date: this.birth_date,
        mobile_number: this.mobile_number,
        country: this.country,
        city: this.city,
        address: this.address,
        email_address: this.email_address,
        avatar: this.avatar,
    }
}
/*
UserSchema.pre('remove', function(next) {
    this.model('Account').remove({user_id: this._id}).exec()
    next();
})*/

UserSchema.index({login: 'text'});

UserSchema.virtual('prefix').get(function () {
    const id = this._id.toString();
    const  prefix = `${id.substr(0,3)}${id.substr(-4)}`;
    return prefix.toUpperCase();
});

module.exports = mongoose.model("User", UserSchema);