const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const NoticeSchema = new Schema(
    {
        user_id: {
            type: Schema.Types.ObjectId, ref: 'User',
            required: true
        },
        type: {type: String, required: true}, // type operation: payment, payment order, etc...
        message: {type: String},
        data: {type: Schema.Types.Mixed, default: {}},
        create_at: {type: Date, default: Date.now},
    },
    {strict: false},
);

NoticeSchema.set('toJSON', {
        virtuals: false,
        versionKey: false,
        transform: (doc, ret) => {
            delete ret._id;
            delete ret.user_id;
        }
    }
);
module.exports = mongoose.model("Notice", NoticeSchema);