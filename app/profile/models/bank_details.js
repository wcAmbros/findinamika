const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const BankDetailsSchema = new Schema(
    {
        user_id: {
            type: Schema.Types.ObjectId, ref: 'User',
            required: true
        },

        /**bank details**/
        account_number:String,
        organization:String,
        INN:String,
        BIC:String,
        KPP:String,
        IBAN:String,
        SWIFT:String,
    }
);

BankDetailsSchema.set('toJSON', {
        virtuals: false,
        versionKey: false,
        transform: (doc, ret) => {
            delete ret._id;
            delete ret.user_id;
        }
    }
);

module.exports = mongoose.model("BankDetails", BankDetailsSchema);