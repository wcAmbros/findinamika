const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const FriendSchema = new Schema(
    {
        friend_id: {
            type: Schema.Types.ObjectId, ref: 'User',
            required: true
        },
        user_id: {
            type: Schema.Types.ObjectId, ref: 'User',
            required: true
        },

    }
);


module.exports = mongoose.model("Friend", FriendSchema);