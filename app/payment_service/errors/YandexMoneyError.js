module.exports = class YandexMoneyError extends Error{
    constructor(response, request) {
        super(response['error']);
        this.response = response;
        this.request = request;

        this.name = 'YandexMoneyError';
        Error.captureStackTrace(this, this.constructor);
    }
}