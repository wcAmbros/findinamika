const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const YandexMoneyJournalSchema = new Schema(
    {
        timestamp: {type: Date, default: Date.now},
        operation_id: String,
        stellar_hash: String,
        type: String, //deposit | withdraw
        status: String,
        user_id: {type: Schema.Types.ObjectId, ref: 'User'},
    }
);

YandexMoneyJournalSchema.set('toJSON', {
            virtuals: false,
            versionKey: false,
            transform: (doc, ret) => {
                    delete ret._id;
                    delete ret.user_id;
            }
    }
);

module.exports = mongoose.model("YandexMoneyJournal", YandexMoneyJournalSchema);
