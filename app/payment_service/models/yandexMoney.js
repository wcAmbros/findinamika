const mongoose = require("mongoose");
const YandexMoneyService = require("../lib/YandexMoneyService");
const Schema = mongoose.Schema;

const YandexMoneySchema = new Schema(
    {
        clientID: String,
        clientSecret: String,
        notificationSecret: String,
        accessToken: String,
        rights: String,
    }
);
YandexMoneySchema.methods.getPaymentService = function (){
    return new YandexMoneyService(this);
}

module.exports = mongoose.model("YandexMoney", YandexMoneySchema);
