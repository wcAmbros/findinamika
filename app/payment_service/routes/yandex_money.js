const express = require("express");
const YandexMoney = require("../models/yandexMoney");
const User = require("app/profile/models/user");
const Account = require("app/stellar/models/account");
const YandexMoneyJournal = require("../models/yandexMoneyJournal");
const appErrorMessage = require("lib/AppErrorMessage");
const appSuccessMessage = require("lib/AppSuccessMessage");
const passportJWT = require("app/profile/middleware/passportJWT");
const {getStellarNameFromAddress} = require("app/stellar/helpers/helper_stellar");
const {deposit} = require("app/stellar/helpers/helper_anchor");
const router = express.Router();

router.post("/notification", async ({body}, res) => {
    try {
        const model = await YandexMoney.findOne();
        const yandexMoneyService = model.getPaymentService();
        const {operation_id} = body;
        let status = 'success';
        let hash = '';
        const typeOperation = 'deposit';
        const rowJournal = await YandexMoneyJournal.findOne({operation_id, status});
        if(!rowJournal && yandexMoneyService.isAccepted(body)){
            const {details, amount}  = await yandexMoneyService.operationDetails(operation_id);
            const name = getStellarNameFromAddress(details);
            const user =  await User.findOne({login:name})
            const account =  await Account.findOne({user_id:user._id});

            if(account){
                const transaction =  await deposit({account,amount, currency:"RUB", memo: typeOperation});
                hash = transaction["hash"];
            }else{
                status = 'hold'
            }
            await YandexMoneyJournal.create({
                operation_id,
                status,
                type: typeOperation,
                stellar_hash: hash
            })
        }
        return res.send();
    } catch (e) {
        return res.status(500).send(appErrorMessage(e));
    }
});

router.post("/setting", passportJWT, async ({body}, res) => {
    try {
        await YandexMoney.deleteOne();
        const model = await YandexMoney.create(body);
        return res.send(appSuccessMessage(model.toObject()));
    } catch (e) {
        return res.status(500).send(appErrorMessage(e));
    }
});


module.exports = router;



