'use strict'
const request = require("request-promise");
const querystring = require('querystring');
const crypto = require('crypto');

/**
 * Класс для работы с api yandex кошелька
 * @see https://yandex.ru/dev/money/doc/dg/concepts/About-docpage/
 *
 * rights - Права на выполнение операций
 * @see https://yandex.ru/dev/money/doc/dg/concepts/protocol-rights-docpage/
 *
 * notificationSecret -  это секретное слово для проверки уведомлений
 * @see https://yandex.ru/dev/money/doc/dg/reference/notification-p2p-incoming-docpage
 * */

class YandexMoneyService {
    /** @type {YandexMoneySchema} */
    constructor(settings) {
        const {clientID, clientSecret, accessToken, rights, notificationSecret} = settings;
        this.clientID = clientID;
        this.clientSecret = clientSecret;
        this.notificationSecret = notificationSecret;
        this.accessToken = accessToken;
        this.rights = rights;
        this.CURRENCY_RUB = "643";
        this.PATTERN_ID = "p2p"
    }

    /** Перевод средств на счет другого пользователя */
    async requestPayment ({to, amount = 0.00, message=""}){
        return requestToYandex({
            uri:"/api/request-payment",
            accessToken:this.accessToken,
            body:{
                pattern_id:this.PATTERN_ID,
                to, amount, message,
                comment:message
            }
        })
    }
    /** Подтверждение перевода */
    async processPayment (request_id){
        return requestToYandex({
            uri:"/api/process-payment",
            accessToken:this.accessToken,
            body:{request_id}
        })
    }
    async accountInfo (){
        return requestToYandex({
            uri:"/api/account-info",
            accessToken:this.accessToken
        })
    }
    async operationHistory (records = 3){
        return requestToYandex({
            uri:"/api/operation-history",
            accessToken:this.accessToken,
            body:{records}
        })
    }
    async operationDetails (operation_id){
        return requestToYandex({
            uri:"/api/operation-details",
            accessToken:this.accessToken,
            body:{operation_id}
        })
    }
    /**
     * Проверка статуса входящего перевода
     * @see https://yandex.ru/dev/money/doc/dg/reference/notification-p2p-incoming-docpage/
     * @param {object}  data
     *
     * return {boolean}
     * */
    isAccepted(data){
        const {unaccepted, codepro} = data;
        return unaccepted == "false" && codepro == "false" && this.checkHash(data)
    }

    /**
     * Проверка hash-строки входящего уведомления
     * @param {object}  data
     *
     * return {boolean}
     * */
    checkHash(data){
        const {sha1_hash = '', notification_type, operation_id, amount, currency, datetime, sender, codepro, label} = data;
        const text = `${notification_type}&${operation_id}&${amount}&${currency}&${datetime}&${sender}&${codepro}&${this.notificationSecret}&${label}`;
        const hash = crypto.createHash('sha1').update(text).digest('hex');
        return hash === sha1_hash;
    }

    getServiceName(){
        return "Yandex Money"
    }

}

const requestToYandex =  async (options)=>{
    const {uri, queryParams, method = "POST", accessToken, body = {}} = options
    const headers = {
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": `Bearer ${accessToken}`
    }

    const result = await request({
        method,
        uri: `https://money.yandex.ru${uri}`,
        qs: queryParams,
        headers,
        body: querystring.stringify(body),
    });

    return JSON.parse(result);
}

module.exports = YandexMoneyService;
