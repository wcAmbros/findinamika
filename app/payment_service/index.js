const express = require("express");

const router = express.Router();

router.use("/yandex-money", require("./routes/yandex_money"));
router.use("/gateline", require("./routes/gateline"));
module.exports = router;