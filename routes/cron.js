const express = require("express");
const {initCurrencyExchange} = require("lib/currencies");
const appErrorMessage = require("lib/AppErrorMessage");
const appSuccessMessage = require("lib/AppSuccessMessage");

const router = express.Router();
let timerId = 0;
const delay = 1000 * 60 * 60;

router.get("/start-exchange", async (req, res) => {
    try {
        await initCurrencyExchange();
        if (timerId) {
            clearInterval(timerId);
        }
        timerId = setInterval( initCurrencyExchange, delay);

        return res.send(appSuccessMessage());

    } catch (e) {
        return res.send(appErrorMessage(e));
    }

});


router.get("/stop-exchange", async (req, res) => {
    try {
        clearInterval(timerId);
        return res.send(appSuccessMessage());

    } catch (e) {
        return res.send(appErrorMessage(e));
    }

});

module.exports = router;



