const express = require("express");
const appErrorMessage = require("lib/AppErrorMessage");

const router = express.Router();

router.post("/receive", async ({body}, res) => {
    try {

        return res.send({});
    } catch (e) {
        return res.status(500).send(appErrorMessage(e));
    }
});

router.post("/error", async ({body}, res) => {
    try {
        return res.send({});

    } catch (e) {
        return res.status(500).send(appErrorMessage(e));
    }
});

module.exports = router;



