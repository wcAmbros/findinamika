/*
* @see https://github.com/stellar/bridge-server/blob/master/readme_compliance.md*/
const express = require("express");

const appErrorMessage = require("lib/AppErrorMessage");
const Account = require("app/stellar/models/account");
const Anchor = require("app/stellar/models/anchor");
const Issuer = require("app/stellar/models/issuer");
const {Server} = require("app/stellar/helpers/helper_stellar");
const  mongoose = require('mongoose');

const router = express.Router();


router.post("/sanctions", async ({body: {sender}}, res) => {
    try {

        return res.send({
            name: sender,
            msg: "allow"
        });
    } catch (e) {
        return res.status(500).send(appErrorMessage(e));
    }
});

router.post("/ask_user", async ({body: {amount, asset_code, asset_issuer, sender, note}}, res) => {
    try {
        const [stellar_name] = sender.split("*");
        const acc = await getAccount(stellar_name);
        if(acc){
            return res.send(getAccountInfo(acc));
        }
        const anchor = await getAnchor(stellar_name);
        if(anchor){
            return res.send(getAnchorInfo(anchor))
        }
        const issuer = await getIssuer(stellar_name);
        if(issuer){
            return res.send(getIssuerInfo(issuer))
        }
        throw new Error('Not found user');
    } catch (e) {
        return res.status(500).send(appErrorMessage(e));
    }
});
router.post("/fetch_info", async ({body: {address}}, res) => {
    try {
        const [stellar_name] = address.split("*");
        const acc = await getAccount(stellar_name);
        if(acc){
            return res.send(getAccountInfo(acc));
        }
        const anchor = await getAnchor(stellar_name);
        if(anchor){
            return res.send(getAnchorInfo(anchor))
        }
        const issuer = await getIssuer(stellar_name);
        if(issuer){
            return res.send(getIssuerInfo(issuer))
        }
        throw new Error('Not found user');
    } catch (e) {
        return res.status(500).send(appErrorMessage(e));
    }
});

/*@see https://github.com/stellar/stellar-protocol/blob/master/ecosystem/sep-0004.md*/
router.post("/tx_status", async ({body: {id}}, res) => {
    try {
        const {records: [record]} = await Server.operations().forTransaction(id).call();
        console.log("/tx_status", {id, record});
        return res.send({
            status: "delivered",
            //refund_tx: "tx_hash",
            msg: "Money has been deposited into the stellar account"
        });
    } catch (e) {
        return res.status(500).send(appErrorMessage(e));
    }
});

module.exports = router;

const getAccount = async (id)=>{
    if(mongoose.Types.ObjectId.isValid(id)){
        return  Account.findOne({_id: id}).populate('user_id');
    }

    return null
}

const getAnchor = async (stellar_name)=>{
    return  Anchor.findOne({stellar_name})
}
const getIssuer = async (currency)=>{
    return  Issuer.findOne({currency})
}

const getAccountInfo = (account)=>{
    const {login="", last_name="", first_name="", additional_name="",country="", city="", address="",email_address=""} = account.user_id;
    return {
        login:  login,
        name: `${last_name} ${first_name} ${additional_name}`.trim(),
        country:  country,
        city:  city,
        address: address,
        email_address: email_address
    }
}

const getAnchorInfo = (anchor)=>{
    if(anchor.organization_info){
        return anchor.organization_info
    }
    return {name:""}
}

const getIssuerInfo = (issuer)=>{
    if(issuer.organization_info){
        return issuer.organization_info
    }
    return {name:""}
}


