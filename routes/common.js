const express = require("express");
const Issuer = require("app/stellar/models/issuer");
const appErrorMessage = require("lib/AppErrorMessage");
const appSuccessMessage = require("lib/AppSuccessMessage");

const router = express.Router();

router.get("/currencies", async (req, res) => {
    try {
        const issuers = await Issuer.find();
        const currencies = issuers.map((item)=>item.getAsset())
        return res.send(appSuccessMessage({currencies}));
    } catch (e) {
        return res.send(appErrorMessage(e));
    }
});

module.exports = router;



