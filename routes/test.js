const express = require("express");
const appErrorMessage = require("lib/AppErrorMessage");
const appSuccessMessage = require("lib/AppSuccessMessage");
const {changeTrust, createWallet} = require("app/stellar/helpers/helper_stellar");
const Account = require("app/stellar/models/account");
const Issuer = require("app/stellar/models/issuer");
const Anchor = require("app/stellar/models/anchor");
const User = require("app/profile/models/user");
const {mainAnchor, federation} = require("config");
//const {Main_Account} = require("cfg/stellar/issuer_assets");

const {deposit} = require("app/stellar/helpers/helper_anchor");
const FederationService = require("app/stellar/lib/FederationService");
const verifyToken = require("app/profile/middleware/passportJWT");
const request = require("request-promise");
const router = express.Router();

router.get("/asset", verifyToken, async ({query: {currency="RUB", amount=300, login}, user}, res) => {
    try {
        let account = null;
        if(login){
            const user2 = await User.findOne({login});
            account = await Account.findOne({user_id: user2.id});
        }else{
            account = await Account.findOne({user_id: user.id});
        }

        if(!account){
            throw new Error(`can't found user: ${login}`)
        }

        const issuers = (currency)? await Issuer.find({currency}): await Issuer.find();
        const assetsCode =[];
        const deposits = issuers.map(async (issuer)=>{
            assetsCode.push(issuer.currency)
            return deposit({account,
                currency:issuer.currency,
                amount, memo: 'test deposit'}
                )
        })
        await Promise.all(deposits);

        return res.send(appSuccessMessage({message: `${amount} (${assetsCode.join(' ')}) tokens transfer(s) to acc`}));
    } catch (e) {
        console.log(e);
        return res.send(appErrorMessage(e));
    }
});
/*
router.post("/anchor/create", async ({session, body}, res) => {
    try {
        const {publicKey, secretKey} = Main_Account;
        const anchor = await Anchor.create({
            publicKey, secretKey, stellar_name: mainAnchor
        })

        return res.send(anchor.toObject());
    } catch (e) {
        return res.status(500).send(appErrorMessage(e));
    }
});
router.post("/anchor/init", async ({session, body}, res) => {
    try {
        const {publicKey} = Main_Account;
        const anchor = await Anchor.findOne({publicKey})
        const issuers = await Issuer.find()
        const data = issuers.map(async (issuer)=>{
            return issuer.mint({
                destination: anchor,
                amount: 1000000,
                memo: 'deposit to main account'
            })
        })

        return res.send(await  Promise.all(data));
    } catch (e) {
        return res.status(500).send(appErrorMessage(e));
    }
});*/

router.post("/anchor/info", async ({session, body}, res) => {
    try {
        const anchor = await Anchor.findOne({stellar_name: mainAnchor});
        anchor.organization_info = body;
        await anchor.save()

        return res.send(anchor.toObject());
    } catch (e) {
        return res.status(500).send(appErrorMessage(e));
    }
});
router.post("/issuer/info", async ({session, body:{currency, info}}, res) => {
    try {
        const issuer = await Issuer.findOne({currency});
        issuer.organization_info = info;
        await issuer.save()
        return res.send(issuer.toObject());
    } catch (e) {
        return res.status(500).send(appErrorMessage(e));
    }
});


/*
router.get("/repair/federation", async (req, res) => {
    try {
        const accounts = await Account.find();
        const fedlist = await request.get({
            url: `${federation}/api/account/all`,
            json: true
        })
        const acc_lst= accounts.filter((row)=>{
            const fedRow = fedlist.find((item)=>item.memo == row._id)
            if(fedRow){
                return false
            }
            return true;
        })

        for(let account of acc_lst ){
            const federationService = new FederationService();
            await federationService.add(account);
        }
        return res.send(acc_lst);
    } catch (e) {
        console.log(e);
        return res.send(appErrorMessage(e));
    }
});
*/
router.get("/repair/accounts", async (req, res) => {
    try {
        const accounts = await Account.find();
        const anchor = await Anchor.findOne({stellar_name: mainAnchor});

        const transactions = accounts.map((account)=>repairAccount(account, anchor));
        return res.send(await Promise.all(transactions));
    } catch (e) {
        console.log(e);
        return res.send(appErrorMessage(e));
    }
});

const repairAccount = async (account, anchor) => {
    await createWallet({publicKey: account.publicKey}, anchor.secretKey)
    const federationService = new FederationService();
    await federationService.add(account);
    const issuer =  await Issuer.findOne({currency:"RUB"})
    return await changeTrust({asset:issuer.getAsset()}, account.secretKey);
}


module.exports = router;



