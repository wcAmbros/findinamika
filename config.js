const config = {
    production:{
        token: {
            secret: "keyboard fd",
            expiresIn: 43200, // expires in 24 hours
        },
        mongo: {
            secret: "keyboard fd",
            mongoString: "mongodb://localhost:27017/stellar",
        },
        stellar_toml: "findinamika.com",
        bridge: "http://bridge2.findinamika.com",
        compliance: "http://compliance2.findinamika.com",
        federation: "http://federation2.findinamika.com",
        node: "https://horizont.findinamika.com",
        mainAnchor: "findinamika",
        usePublicNetwork: true
    }
};

/** Чтобы JSDoc не ругался */
module.exports = config.production;
//module.exports = config[process.env.NODE_ENV];