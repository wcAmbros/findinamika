require("app-module-path/register");
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const { MongoMemoryServer } = require("mongodb-memory-server-core");
const helmet = require("helmet");
const session = require("express-session");
const connectMongo = require("connect-mongo");
const cors = require("cors");
const config = require("./config");
const autoIncrement = require("mongoose-auto-increment");
const passport = require("passport");

// set up express app
const app = express();

const mongoServer = new MongoMemoryServer();

mongoose.Promise = global.Promise;
mongoServer.getConnectionString().then(async (mongoUri) => {

    await mongoose.connect(mongoUri, {useNewUrlParser: true, useCreateIndex: true}, (e) => {
        if (e) console.error(e);
    });

    console.log(`MongoDB successfully connected to ${mongoUri}`);

    autoIncrement.initialize(mongoose.connection);

    const MongoStore = connectMongo(session);
    app.use(session({
        secret: config.mongo.secret,
        resave: false,
        saveUninitialized: false,
        cookie: {secure: true},
        store: new MongoStore({ mongooseConnection: mongoose.connection })
    }))

    app.use(cors());

    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    passport.deserializeUser((sessionUser, done) => {
        done(null, sessionUser);
    });
    app.use(passport.initialize());
    app.use(passport.session());

    app.use(helmet());

// use body-parser middleware
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));

// initialize routes
    app.use("/api", require("./app/index"));

    app.use(function (req, res) {
        res.status(404).send({error: {name: "Error", message: "Unused request. Check request method"}});
    });

// listen for requests
    app.listen(process.env.port || 4000, () => {
        console.log(`server mode: ${process.env.NODE_ENV}`);
        console.log(`now listening for requests on port ${process.env.port || 4000}`);
    });
});