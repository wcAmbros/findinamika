require("app-module-path/register");
const express = require("express");
const { createServer } = require("http");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const helmet = require("helmet");
const session = require("express-session");
const connectMongo = require("connect-mongo");
const cors = require("cors");
const config = require("./config");
const autoIncrement = require("mongoose-auto-increment");
const passport = require("passport");
const socket =  require("./app/websocket/helpers/socket");

// set up express app
const app = express();
const server = createServer(app);

socket.server = server;

const wss = socket.init('/notice/', require('./app/websocket/notice'));

app.use(function (req, res, next) {
    req.wss = wss;
    next();
});

mongoose.connect(config.mongo.mongoString, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true
}, function () {
    /* Drop the DB*/

    //mongoose.connection.collections['services'].drop();
    //mongoose.connection.db.dropDatabase();

});

autoIncrement.initialize(mongoose.connection);
mongoose.Promise = global.Promise;

const MongoStore = connectMongo(session);
app.use(cors());
app.use(session({
    secret: config.mongo.secret,
    resave: false,
    saveUninitialized: false,
    cookie: {secure: true},
    store: new MongoStore({
        url: config.mongo.mongoString,
    })
}))

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser((sessionUser, done) => {
    done(null, sessionUser);
});
app.use(passport.initialize());
app.use(passport.session());

app.use(helmet());

// use body-parser middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// initialize routes
app.use("/api", require("./app/index"));

app.use(function (req, res) {
    res.status(404).send({error: {name: "Error", message: "Unused request. Check request method"}});
});

// listen for requests
server.listen(process.env.port || 4000, () => {
    console.log(`server mode: ${process.env.NODE_ENV}`);
    console.log(`now listening for requests on port ${process.env.port || 4000}`);
});


