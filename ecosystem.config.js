module.exports = {
  apps : [{
    name: "app",
    script: "./index.js",
    watch: true,
    env: {
      NODE_ENV: "production",
    },
    node_args: ["--inspect"],
  }],
};
