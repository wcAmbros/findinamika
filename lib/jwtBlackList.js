const nodeCache = require( "node-cache");
const config = require( "../config");
const cache = new nodeCache();

module.exports = {
    add: (token) => {
        cache.set(token, {type:"jwtBlacklist"}, config.token.expiresIn);
    },
    verify: async (token) => {
        const res = await cache.get(token);
        if (res) {
            throw new Error("token in blacklist");
        }
    }
};