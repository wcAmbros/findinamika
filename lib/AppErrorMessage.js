const TransactionError = require("app/stellar/errors/TransactionError");

function ValidationErrorMessage(e) {
    const mess = e.errors.map(({message})=>message);
    return mess.join();
}


module.exports = (e) => {
    const {name, message} = e
    console.log(e);
    if(e instanceof TransactionError){
        return {error: {name, message: e.data}};
    }
    if(name == 'ValidationError'){
        return {error: {name, message: ValidationErrorMessage(e)}};
    }
    return {error: {name, message}};
}