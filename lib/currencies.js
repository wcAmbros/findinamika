const request = require("request-promise");
const {Issuer_Assets} = require("cfg/stellar/issuer_assets");
const {submitTransaction, getAsset, changeTrust} = require("app/stellar/helpers/helper_stellar");
const {getAnchor} = require("app/stellar/helpers/helper_anchor");
const {BigNumber} = require("bignumber.js");

const getCurrencies = async () => {
    const {base, rates} = await request({
        uri: "https://openexchangerates.org/api/latest.json",
        qs: {app_id: "5e24ce08c2524245824341cce5d894bf"},
        json: true
    });

    const data = {};
    data[base] = rates[base];
    for (let currency in Issuer_Assets) {
        if (rates[currency]) {
            data[currency] = rates[currency];
        }
    }
    return data;
};

const buyAsset = async (asset, currencies, amount) => {

    const anchor = getAnchor(asset);

    try {
        await anchor.mint(amount * Object.keys(currencies).length);
    } catch (e) {
        //await anchor.initIssuer(1000000);
        await anchor.mint(amount * Object.keys(currencies).length);
    }

    const operations = [];
    for (let buying in currencies) {
        if (asset == buying) {
            continue
        }


        let price = new BigNumber(currencies[buying]);

        if (currencies[asset] != 1) {
            price = price.dividedBy(currencies[asset]);
        }

        operations.push(
            await anchor.buyOffer({
                amount,
                buying,
                price,
                onlyOperation: true
            })
        );

    }

    const transaction = await submitTransaction({
        memo: "buy asset",
        operations: operations,
        secretKey: anchor.base.secretKey
    });

    console.log("buy asset", transaction);

}

const sellAsset = async (asset, currencies, amount) => {

    const anchor = getAnchor(asset);

    const operations = [];
    for (let selling in currencies) {
        if (asset == selling) {
            continue
        }

        let price = new BigNumber(currencies[selling]);

        if (currencies[asset] != 1) {
            price = price.dividedBy(currencies[asset]);
        }

        operations.push(
            await anchor.sellOffer({
                amount,
                selling,
                price,
                onlyOperation: true
            })
        );

    }

    const transaction = await submitTransaction({
        memo: "sell asset",
        operations: operations,
        secretKey: anchor.base.secretKey
    });
    console.log("sell asset", transaction);
}

const initCurrencyExchange = async () => {

    const currencies = await getCurrencies();

    //await initAnchors(currencies);
    /*for (let asset in currencies) {

        await buyAsset(asset, currencies, 2000);
    }*/

    await buyAsset("RUB", currencies, 10000);


}

const initAnchors = async (currencies) => {


    for (let currency in currencies) {
        const anchor = getAnchor(currency);
        await anchor.initIssuer(1000000);
    }

    for (let currency in currencies) {
        const anchor = getAnchor(currency);
        for (let asset in currencies) {
            if (asset == currency) {
                continue
            }

            await changeTrust({asset:await getAsset(asset), limit:100000}, anchor.base.secretKey);

            console.log({currency,asset});

        }
    }

}
module.exports = {initCurrencyExchange}