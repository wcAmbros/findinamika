const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
chai.use(chaiHttp);

/***
 * To set max 'timeout' for a test,
 * need start Mocha with '--timeout 30000' (30 sec).
 * @see https://mochajs.org/#test-level
 * @see https://github.com/nodkz/mongodb-memory-server
 * */
before(function (done) {
    setTimeout(done, 3000); // await init server
});

describe('Test route: /api/user', function () {
    const APIserver = chai.request(`http://localhost:${process.env.port}`);

    it('GET /api/user/signup', async () => {
        const res = await APIserver.post('/api/user/signup').send({
            login:"test",
            password:"test",
        });
        expect(res).to.have.status(200);
    });
});